import json
import boto3


snsclient = boto3.client('sns')
ec2client = boto3.client('ec2')


snsTopicARN = "arn:aws:sns:ca-central-1:893754988475:New-SNS-TopicAlarm"


def lambda_handler(event, context):

    instanceID = event['detail']['instance-id']

    #Instance Information
    instanceInformation = ec2client.describe_instances(
        InstanceIds=[
           instanceID
        ],
    )

    instance = instanceInformation['Reservations'][0]['Instances'][0]


    # Extract name tag
    name_tags = [t['Value'] for t in instance['Tags'] if t['Key']=='Name']
    instanceName = name_tags[0] if name_tags is not None else ''
    instanceState = instance['State']['Name']


    #Send Message to SNS Topic
    snsclient.publish(
        TopicArn = snsTopicARN,
        Subject = 'EC2 Instance State-change Notification for: ' + instanceName + ' is ' + instanceState,
        Message = 'Instance Details:\n' + 'Instance ID: ' + instanceID + '\n' + 'State: ' + instance['State']['Name'] + '\n' + 'Instance Name: ' + instanceName
    )
